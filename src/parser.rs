// Copyright (C) 2022 Maxime “pep” Buquet <pep@bouah.net>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::error::Error;
use crate::types::{Action, Card, Color, ColorDigit, Digit, GameAction, ReplAction, Slot, Slots};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{digit1, space0, space1},
    error::{Error as NomError, ErrorKind},
    multi::many0,
    IResult,
};
use std::convert::TryFrom;
use std::str::FromStr;

fn parse_color(i: &str) -> IResult<&str, ColorDigit> {
    let (i, color) = alt((
        tag("blue"),
        tag("b"),
        tag("green"),
        tag("g"),
        tag("purple"),
        tag("p"),
        tag("red"),
        tag("r"),
        tag("white"),
        tag("w"),
        tag("yellow"),
        tag("y"),
    ))(i)?;
    let color = Color::from_str(color).unwrap();
    Ok((i, color.into()))
}

fn parse_digit(i: &str) -> IResult<&str, ColorDigit> {
    let (i, digit) = alt((
        tag("1"),
        tag("one"),
        tag("2"),
        tag("two"),
        tag("3"),
        tag("three"),
        tag("4"),
        tag("four"),
        tag("5"),
        tag("five"),
    ))(i)?;
    let digit = Digit::from_str(digit).unwrap();
    Ok((i, digit.into()))
}

fn parse_card(i: &str) -> IResult<&str, Card> {
    let (i, color) = parse_color(i)?;
    let (i, _) = space0(i)?;
    let (i, digit) = parse_digit(i)?;
    Ok((
        i,
        Card {
            digit: Digit::try_from(digit).unwrap(),
            color: Color::try_from(color).unwrap(),
        },
    ))
}

fn parse_play(i: &str) -> IResult<&str, GameAction> {
    let (i, _) = alt((tag("play"), tag("p")))(i)?;
    let (i, _) = space1(i)?;
    let (i, card) = parse_card(i)?;
    let (i, _) = space1(i)?;
    let (i, slot) = parse_slot(i)?;

    Ok((i, GameAction::PlayCard(card, slot)))
}

fn parse_slot(i: &str) -> IResult<&str, Slot> {
    let (i, slot) = digit1(i)?;
    let slot = slot
        .parse::<u8>()
        .map_err(|_| nom::Err::Error(NomError::new("", ErrorKind::IsA)))?;

    if slot < 1 || slot > 6 {
        return Err(nom::Err::Error(NomError::new("", ErrorKind::IsA)));
    }

    Ok((i, slot))
}

fn parse_slot1(i: &str) -> IResult<&str, Slot> {
    let (i, _) = space1(i)?;
    let (i, slot) = parse_slot(i)?;

    Ok((i, slot))
}

fn parse_slots1(i: &str) -> IResult<&str, Slots> {
    let (i, slot1) = parse_slot(i)?;
    let (i, slots2) = many0(parse_slot1)(i)?;

    let mut slots = vec![slot1];
    slots.extend(slots2);

    Ok((i, slots.into()))
}

fn parse_drop(i: &str) -> IResult<&str, GameAction> {
    let (i, _) = alt((tag("drop"), tag("d")))(i)?;
    let (i, _) = space1(i)?;
    let (i, card) = parse_card(i)?;
    let (i, _) = space1(i)?;
    let (i, slot) = parse_slot(i)?;

    Ok((i, GameAction::DropCard(card, slot)))
}

fn parse_hint(i: &str) -> IResult<&str, GameAction> {
    let (i, _) = alt((tag("hint"), tag("h")))(i)?;
    let (i, _) = space1(i)?;
    let (i, colordigit) = alt((parse_color, parse_digit))(i)?;
    let (i, _) = space1(i)?;
    let (i, slots) = parse_slots1(i)?;

    Ok((i, GameAction::from((colordigit, slots))))
}

fn parse_gameaction(i: &str) -> IResult<&str, Action> {
    let (i, action) = alt((parse_play, parse_drop, parse_hint))(i)?;
    Ok((i, Action::Game(action)))
}

fn parse_replcancel(i: &str) -> IResult<&str, ReplAction> {
    let (i, _action) = tag("cancel")(i)?;
    Ok((i, ReplAction::CancelLast))
}

fn parse_repldisplay(i: &str) -> IResult<&str, ReplAction> {
    let (i, _action) = tag("display")(i)?;
    Ok((i, ReplAction::DisplayBoard))
}

fn parse_replaction(i: &str) -> IResult<&str, Action> {
    let (i, action) = alt((parse_replcancel, parse_repldisplay))(i)?;
    Ok((i, Action::Repl(action)))
}

fn parse_action(i: &str) -> IResult<&str, Action> {
    let (i, action) = alt((parse_gameaction, parse_replaction))(i)?;
    Ok((i, action))
}

pub fn parse_line(input: &str) -> Result<Action, Error> {
    Ok(parse_action(input)?.1)
}
