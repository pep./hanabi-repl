// Copyright (C) 2022 Maxime “pep” Buquet <pep@bouah.net>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

mod args;
mod error;
mod parser;
mod types;

use crate::args::Args;
use crate::error::Error;
use crate::parser::parse_line;
use crate::types::{Action, GameAction, GameState, ReplAction, Variant};

use std::fs::File;
use std::io::Write;

use chrono::{DateTime, Utc};
use clap::Parser;
use env_logger;
use rustyline::{error::ReadlineError, Editor};

fn write_header(filename: &str, players: u8, variants: Vec<Variant>) -> Result<(), Error> {
    let mut file = File::create(filename)?;
    let variants = variants.iter().map(|variant| variant.to_string()).fold(
        String::new(),
        |mut res, variant| {
            if res.len() == 0 {
                res.push_str(variant.as_str());
            } else {
                res.push_str(format!(", {}", variant).as_str());
            }

            res
        },
    );
    let _ = file.write(format!("hanabi: {}p; {}\n", players, variants).as_bytes())?;
    Ok(())
}

fn write_actions(filename: &str, action: GameAction) -> Result<(), Error> {
    let mut file = File::options().append(true).open(filename)?;
    let buf = format!("{}\n", action.to_string());
    let _ = file.write(buf.as_bytes())?;
    Ok(())
}

fn main() -> Result<(), Error> {
    env_logger::init();

    let args = Args::parse();
    println!(
        "Hanabi - Players: {} - Variants: {}",
        args.players, args.variant
    );

    let dt: DateTime<Utc> = Utc::now();
    let filename = format!(
        "hanabi_{}p_{}.txt",
        args.players,
        dt.format("%Y-%m-%d_%H:%M:%S")
    );
    println!("Recording at: {}", filename);

    // TODO: Move writing to GameState
    write_header(filename.clone().as_str(), args.players, vec![args.variant])?;

    let mut rl = Editor::<()>::new()?;
    let mut game: GameState = GameState::new(args.players, vec![args.variant]);

    loop {
        let readline = rl.readline(">> ");
        match readline {
            Ok(line) => {
                rl.add_history_entry(line.as_str());
                if let Ok(action) = parse_line(line.as_str()) {
                    match action {
                        Action::Game(action) => {
                            game.add_action(action.clone());
                            // TODO: Move writing to GameState
                            write_actions(filename.clone().as_str(), action)?
                        }
                        Action::Repl(ReplAction::DisplayBoard) => game.show(),
                        _ => (),
                    }
                } else {
                    println!("Invalid command: {}", line);
                }
            }
            Err(ReadlineError::Interrupted) | Err(ReadlineError::Eof) => {
                println!("Quitting.");
                break;
            }
            Err(err) => {
                println!("Error: {:?}", err);
                break;
            }
        }
    }
    Ok(())
}
