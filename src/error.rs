// Copyright (C) 2022 Maxime “pep” Buquet <pep@bouah.net>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use nom;
use rustyline::error::ReadlineError;
use std::error::Error as StdError;
use std::fmt;
use std::io::Error as IOError;

#[derive(Debug)]
pub enum Error {
    IOError(IOError),
    ParseColorError(String),
    ParseDigitError(String),
    NomError(nom::Err<nom::error::Error<String>>),
    ReadlineError(ReadlineError),
}

impl StdError for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::IOError(err) => write!(f, "IO error: {}", err),
            Error::ParseDigitError(err) => write!(f, "Parse digit error: {}", err),
            Error::ParseColorError(err) => write!(f, "Parse color error: {}", err),
            Error::NomError(err) => write!(f, "Nom error: {}", err),
            Error::ReadlineError(err) => write!(f, "Readline error: {}", err),
        }
    }
}

impl<'a> From<nom::Err<nom::error::Error<&'a str>>> for Error {
    fn from(err: nom::Err<nom::error::Error<&'a str>>) -> Error {
        let foo = match err {
            nom::Err::Incomplete(needed) => nom::Err::Incomplete(needed),
            nom::Err::Error(error) => nom::Err::Error(nom::error::Error::new(
                String::from(error.input),
                error.code,
            )),
            nom::Err::Failure(error) => nom::Err::Failure(nom::error::Error::new(
                String::from(error.input),
                error.code,
            )),
        };
        Error::NomError(foo)
    }
}

impl From<IOError> for Error {
    fn from(err: IOError) -> Error {
        Error::IOError(err)
    }
}

impl From<ReadlineError> for Error {
    fn from(err: ReadlineError) -> Error {
        Error::ReadlineError(err)
    }
}
