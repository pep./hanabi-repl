// Copyright (C) 2022 Maxime “pep” Buquet <pep@bouah.net>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use std::convert::TryFrom;
use std::collections::HashMap;
use std::fmt;
use std::ops::Deref;
use std::str::FromStr;

use crate::error::Error;

use clap::ArgEnum;

/// Game variant. Variants change rules and may be combined
#[derive(Copy, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, ArgEnum)]
pub enum Variant {
    #[default]
    /// Default variant, 5 colors 10 cards
    Default,
    /// Multicolor variant, 6th color, 10 cards
    Multicolor,
}

impl fmt::Display for Variant {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Variant::Default => "default",
                Variant::Multicolor => "multicolor",
            }
        )
    }
}

/// Digit values a card can take
#[derive(Clone, Debug, Hash, Eq, PartialEq)]
pub enum Digit {
    One,
    Two,
    Three,
    Four,
    Five,
}

impl FromStr for Digit {
    type Err = Error;

    fn from_str(s: &str) -> Result<Digit, Error> {
        let s = s.trim().to_lowercase();
        Ok(match s.as_str() {
            "1" | "one" => Digit::One,
            "2" | "two" => Digit::Two,
            "3" | "three" => Digit::Three,
            "4" | "four" => Digit::Four,
            "5" | "five" => Digit::Five,
            _ => return Err(Error::ParseDigitError(s.clone())),
        })
    }
}

impl fmt::Display for Digit {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Digit::One => "1",
                Digit::Two => "2",
                Digit::Three => "3",
                Digit::Four => "4",
                Digit::Five => "5",
            }
        )
    }
}

impl From<Digit> for u8 {
    fn from(digit: Digit) -> u8 {
        match digit {
            Digit::One => 1,
            Digit::Two => 2,
            Digit::Three => 3,
            Digit::Four => 4,
            Digit::Five => 5,
        }
    }
}

/// Colors a card can take
#[derive(Clone, Debug, Hash, Eq, PartialEq)]
pub enum Color {
    Blue,
    Green,
    Purple,
    Red,
    White,
    Yellow,
}

impl FromStr for Color {
    type Err = Error;

    fn from_str(s: &str) -> Result<Color, Error> {
        let s = s.trim().to_lowercase();
        Ok(match s.as_str() {
            "b" | "blue" => Color::Blue,
            "g" | "green" => Color::Green,
            "p" | "purple" => Color::Purple,
            "r" | "red" => Color::Red,
            "w" | "white" => Color::White,
            "y" | "yellow" => Color::Yellow,
            _ => return Err(Error::ParseColorError(s.clone())),
        })
    }
}

impl fmt::Display for Color {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Color::Blue => "b",
                Color::Green => "g",
                Color::Purple => "p",
                Color::Red => "r",
                Color::White => "w",
                Color::Yellow => "y",
            }
        )
    }
}

/// Helper for parser
#[derive(Clone, Debug, PartialEq)]
pub enum ColorDigit {
    Color(Color),
    Digit(Digit),
}

impl From<Color> for ColorDigit {
    fn from(c: Color) -> Self {
        ColorDigit::Color(c)
    }
}

impl From<Digit> for ColorDigit {
    fn from(d: Digit) -> Self {
        ColorDigit::Digit(d)
    }
}

impl TryFrom<ColorDigit> for Digit {
    type Error = Error;

    fn try_from(cd: ColorDigit) -> Result<Digit, Self::Error> {
        Ok(match cd {
            ColorDigit::Digit(d) => d,
            ColorDigit::Color(c) => return Err(Error::ParseDigitError(c.to_string())),
        })
    }
}

impl TryFrom<ColorDigit> for Color {
    type Error = Error;

    fn try_from(cd: ColorDigit) -> Result<Color, Self::Error> {
        Ok(match cd {
            ColorDigit::Color(c) => c,
            ColorDigit::Digit(d) => return Err(Error::ParseColorError(d.to_string())),
        })
    }
}

/// A card
#[derive(Clone, Debug, Hash, Eq, PartialEq)]
pub struct Card {
    pub digit: Digit,
    pub color: Color,
}

impl fmt::Display for Card {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}{}",
            match self.color {
                Color::Blue => "b",
                Color::Green => "g",
                Color::Purple => "p",
                Color::Red => "r",
                Color::White => "w",
                Color::Yellow => "y",
            },
            self.digit
        )
    }
}

/// Position in a player's hand
pub type Slot = u8;

/// Positions in a player's hand
#[derive(Clone, Debug, Hash, Eq, PartialEq)]
pub struct Slots(Vec<Slot>);

impl Deref for Slots {
    type Target = Vec<Slot>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl From<Vec<Slot>> for Slots {
    fn from(s: Vec<Slot>) -> Slots {
        Slots(s)
    }
}

impl fmt::Display for Slots {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.len() > 0 {
            write!(f, "{}", self[0])?;
        }

        if self.len() > 1 {
            for slot in self.iter() {
                write!(f, " {}", slot)?;
            }
        }

        Ok(())
    }
}

/// Possible actions during the game
#[derive(Clone, Debug, Hash, Eq, PartialEq)]
pub enum GameAction {
    PlayCard(Card, Slot),
    DropCard(Card, Slot),
    ColorHint(Slots, Color),
    DigitHint(Slots, Digit),
}

impl From<(ColorDigit, Slots)> for GameAction {
    fn from((cd, slots): (ColorDigit, Slots)) -> Self {
        match cd {
            ColorDigit::Color(c) => GameAction::ColorHint(slots, c),
            ColorDigit::Digit(d) => GameAction::DigitHint(slots, d),
        }
    }
}

impl fmt::Display for GameAction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            GameAction::PlayCard(card, slot) => write!(f, "play {} {}", card, slot),
            GameAction::DropCard(card, slot) => write!(f, "drop {} {}", card, slot),
            GameAction::ColorHint(slots, c) => write!(f, "hint {} {}", c, slots),
            GameAction::DigitHint(slots, d) => write!(f, "hint {} {}", d, slots),
        }
    }
}

/// Possible actions proper to the REPL
#[derive(Clone, Debug, PartialEq)]
pub enum ReplAction {
    DisplayBoard,
    CancelLast,
}

/// Complete set of actions available to users of the program
#[derive(Clone, Debug, PartialEq)]
pub enum Action {
    Game(GameAction),
    Repl(ReplAction),
}

/// Game state
#[derive(Clone, Debug, PartialEq)]
pub struct GameState {
    /// Number of players
    players: u8,
    /// Variants used in the game
    variants: Vec<Variant>,
    /// Actions played
    actions: Vec<GameAction>,
    /// Playable colors
    colors: Vec<Color>,
    /// Total number of cards in starting deck
    total_cards: u8,
    /// Hand capacity
    hand_capacity: u8,
    /// Known cards in hands
    hands: Vec<Vec<Option<Card>>>,
    /// Discard pile
    discarded_cards: Vec<Card>,
    /// Cards played on the board
    played_cards: HashMap<Color, Card>,
}

impl GameState {
    pub fn new(players: u8, variants: Vec<Variant>) -> GameState {
        // TODO: Return Result
        if players > 5 {
            panic!("Players can only be up to 5");
        }

        // XXX: Varies depending on variant
        let hand_capacity: u8 = match players {
            2 => 6,
            3 => 5,
            4..=5 => 4,
            _ => unreachable!(),
        };

        let hands: Vec<Vec<Option<Card>>> = {
            let mut hands = Vec::new();
            let mut hand = Vec::with_capacity(usize::from(hand_capacity));
            (1..=hand_capacity).for_each(|_| hand.push(None));
            (1..=players).for_each(|_| hands.push(hand.clone()));
            hands
        };

        let colors = vec![
            Color::Blue,
            Color::Green,
            Color::Red,
            Color::White,
            Color::Yellow,
        ];

        let total_cards = 50u8;

        GameState {
            players,
            variants,
            actions: vec![],
            colors,
            total_cards,
            hand_capacity,
            hands,
            discarded_cards: vec![],
            played_cards: HashMap::new(),
        }
    }

    pub fn add_action(&mut self, action: GameAction) {
        self.actions.push(action);
    }

    pub fn show(&self) {
        // Color board.
        // Static letters
        self.colors.iter().for_each(|color| {
           print!("{} ", color);
        });
        println!();

        // Ranks
        self.colors.iter().enumerate().for_each(|(i, color)| {
            if let Some(card) = self.played_cards.get(color) {
                print!("{} ", card);
                if i == self.colors.len() - 1 {
                    println!();
                }
            } else {
                print!("x ");
            }
        });
        print!("\n\n");

        // Player card hints
        (1..=self.players).into_iter().for_each(|player| {
            print!("Player {}: ", player);
            let idx = usize::from(player) - 1;
            self.hands[idx].iter().for_each(|card| {
                if let Some(card) = card {
                    print!("{} ", card);
                } else {
                    print!("xx ");
                }
            });
            println!();
        });
        println!();

        // Drawing deck, number of cards remaining
        let total = {
            let played = self.colors.iter().fold(0u8, |res, color| {
                res + self.played_cards.get(color).map(|card| u8::from(card.digit.clone())).unwrap_or(0)
            });
            let in_hands = self.players * self.hand_capacity;
            self.total_cards - in_hands - played - u8::try_from(self.discarded_cards.len()).unwrap()
        };
        println!("Deck: {} card(s) remaining", total);

        // Discard pile, number of cards, cards included in
        print!("Discard: ");
        self.discarded_cards.iter().for_each(|card| {
            print!("{} ", card);
        });
        print!("\n\n");

         // Player turn
         // TODO: End game
        println!("Player {} to go.", self.actions.len() % usize::from(self.players) + 1);
    }
}
